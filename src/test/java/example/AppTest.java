package example;

import graddle.example.App;
import org.junit.Assert;
import org.junit.Test;


public class AppTest {
    @Test
    public void testHello(){
        App.main(new String[]{});
        Assert.assertEquals("Hello world!", "Hello world!");
    }
    @Test
    public void testHelloSecond(){
        App.main(new String[]{});
        Assert.assertEquals("Hello!", "Hello world!");
    }
}
